/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Picture
 */

#include "Picture.h"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include <QDebug>

Picture::Picture(QObject* parent)
  : QObject(parent)
  , _Valid(false)
{
}

Picture::~Picture()
{
}

void Picture::changePicture(const QString & inFilePath)
{
  _Img = cv::imread(inFilePath.toLocal8Bit().data(), cv::IMREAD_UNCHANGED);
  _Valid = true;
  emit pictureChanged();
}

void Picture::setMat(const cv::Mat & inMat)
{
  _Img = inMat.clone();
  emit pictureUpdated();
}

cv::Mat Picture::getMat()
{
  return _Img;
}

QImage Picture::cvMatToQImage()
{
  if (!_Valid)
  {
    return QImage();
  }
  switch (_Img.type())
  {
    // 8-bit, 4 channel
  case CV_8UC4:
  {
    QImage image(_Img.data,
      _Img.cols, _Img.rows,
      static_cast<int>(_Img.step),
      QImage::Format_ARGB32);

    return image;
  }

  // 8-bit, 3 channel
  case CV_8UC3:
  {
    QImage image(_Img.data,
      _Img.cols, _Img.rows,
      static_cast<int>(_Img.step),
      QImage::Format_RGB888);

    return image.rgbSwapped();
  }

  // 8-bit, 1 channel
  case CV_8UC1:
  {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    QImage image(_Img.data,
      _Img.cols, _Img.rows,
      static_cast<int>(_Img.step),
      QImage::Format_Grayscale8);
#else
    static QVector<QRgb>  sColorTable;

    // only create our color table the first time
    if (sColorTable.isEmpty())
    {
      sColorTable.resize(256);

      for (int i = 0; i < 256; ++i)
      {
        sColorTable[i] = qRgb(i, i, i);
      }
    }

    QImage image(_Img.data,
      _Img.cols, _Img.rows,
      static_cast<int>(_Img.step),
      QImage::Format_Indexed8);

    image.setColorTable(sColorTable);
#endif

    return image;
  }

  default:
    qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << _Img.type();
    break;
  }

  return QImage();
}

QPixmap Picture::cvMatToQPixmap()
{
  return QPixmap::fromImage(cvMatToQImage());
}
