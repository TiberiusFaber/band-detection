/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Picture
 * Details: Holds an OpenCV image. Easy to show in a Qt application
 *          and it will send a signal if the image is changed.
 */

#ifndef PICTURE_H
#define PICTURE_H

#include <QObject>
#include <QString>
#include <QImage>
#include <QPixmap>

#include <opencv2/core/core.hpp>

class Picture : public QObject
{
  Q_OBJECT

public:
  Picture(QObject* parent = nullptr);
  ~Picture();

  void changePicture(const QString& inFilePath);

  void setMat(const cv::Mat& inMat);

  cv::Mat getMat();
  QImage cvMatToQImage();
  QPixmap cvMatToQPixmap();

signals:
  void pictureChanged();
  void pictureUpdated();

private:
  bool _Valid;
  cv::Mat _Img;

};

#endif