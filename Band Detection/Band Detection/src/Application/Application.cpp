/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Application
 */

#include "Application.h"

#include <QString>
#include <QFileDialog>

Application::Application(QWidget *parent)
  : QMainWindow(parent)
  , _ProcessManager(std::make_unique<ProcessManager>())
  , _Picture(std::make_shared<Picture>())
{
  _Ui.setupUi(this);

  connect(_Ui.OpenImage_PB, &QPushButton::clicked, this, &Application::openImage);
  connect(_Picture.get(), SIGNAL(pictureUpdated()), this, SLOT(pictureUpdated()));

  _ProcessManager->setup(_Ui.ProcessWidgetCollector_T, _Picture);
}

void Application::openImage()
{
  QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), QString(), tr("Image Files (*.png *.jpg *.jpeg *.bmp)"));
  _Picture->changePicture(fileName);
  _Ui.ImageViewer_L->setPixmap(_Picture->cvMatToQPixmap());
}

void Application::pictureUpdated()
{
  _Ui.ImageViewer_L->setPixmap(_Picture->cvMatToQPixmap());
}
