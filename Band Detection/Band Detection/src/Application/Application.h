/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   Application
 * Details: Setup the program opens an image and shows it.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QtWidgets/QMainWindow>
#include "ui_Application.h"

#include <memory>

#include "../Picture/Picture.h"
#include "../Process/ProcessManager/ProcessManager.h"

class Application : public QMainWindow
{
  Q_OBJECT

public:
  Application(QWidget *parent = Q_NULLPTR);

private slots:
  void openImage();
  void pictureUpdated();

private:
  std::unique_ptr<ProcessManager> _ProcessManager;
  Ui::ApplicationClass _Ui;
  std::shared_ptr<Picture> _Picture;
};

#endif