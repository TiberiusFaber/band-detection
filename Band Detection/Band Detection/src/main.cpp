/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Details: Interview task, create a well structured software which
 *          loads an image, shows on the GUI and show horizontal lines
 *          according to some certain parameters.
 */

#include "Application/Application.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Application w;
    w.showMaximized();
    return a.exec();
}
