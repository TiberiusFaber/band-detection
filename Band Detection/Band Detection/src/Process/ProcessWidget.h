/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   ProcessWidget
 * Details: General process widget class. Called by the process manager.
 *          Abstract class, needs to be inherited.
 */

#ifndef PROCESS_WIDGET_H
#define PROCESS_WIDGET_H

#include "../Picture/Picture.h"

#include <QWidget>
#include <QString>

#include <memory>

namespace process
{

class ProcessWidget : public QWidget
{
  Q_OBJECT

public:
  ProcessWidget(const QString& inProcessName, std::shared_ptr<Picture> inPicture, QWidget* parent = nullptr)
    : QWidget(parent)
    , _ProcessName(inProcessName)
    , _Picture(inPicture)
  {}

  virtual bool execute() = 0;

  QString getName() const { return _ProcessName; }

protected:
  std::shared_ptr<Picture> _Picture;

private:
  QString _ProcessName;
};

}

typedef QMap<QString, process::ProcessWidget> ProcessWidgetMap;

#endif
