/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   BandDetectorWidget
 * Details: A process. Finds the bands on a phase contrast
 *          test slide.
 */

#ifndef BAND_DETECTOR_WIDGET_H
#define BAND_DETECTOR_WIDGET_H

#include "../ProcessWidget.h"
#include "../../Picture/Picture.h"

#include <QWidget>
#include "ui_BandDetectorWidget.h"

#include <memory>

namespace process
{

class BandDetectorWidget : public ProcessWidget
{
  Q_OBJECT

public:
  BandDetectorWidget(std::shared_ptr<Picture> inPicture, QWidget* parent = nullptr);
  ~BandDetectorWidget();

  virtual bool execute();

private slots:
  void distanceChanged(const int inDistance);

  void numberOfMinimumLinesChanged(const int inNumberOfMinimumLines);

  void blendingChanged(const int inBlending);

  void pictureChanged();

private:
  void compute();

private:
  Ui::BandDetectorWidget _Ui;

  int _Distance;
  int _NumOfMinLines;
  float _Blending;

  cv::Mat _OriginalImage;
};

}

#endif // !BAND_DETECTOR_WIDGET_H