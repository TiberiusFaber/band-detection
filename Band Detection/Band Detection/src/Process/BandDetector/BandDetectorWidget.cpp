/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   BandDetectorWidget
 */

#include "BandDetectorWidget.h"

#include <QSpinBox>
#include <QSlider>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include <cmath>
#include <utility>

namespace process
{

BandDetectorWidget::BandDetectorWidget(std::shared_ptr<Picture> inPicture, QWidget* parent)
    : ProcessWidget("Band Detection", inPicture, parent)
  , _Blending(50)
{
  _Ui.setupUi(this);

  connect(_Ui.Distance_SB, SIGNAL(valueChanged(int)), _Ui.Distance_S, SLOT(setValue(int)));
  connect(_Ui.Distance_S, SIGNAL(valueChanged(int)), _Ui.Distance_SB, SLOT(setValue(int)));

  connect(_Ui.Blending_S, SIGNAL(valueChanged(int)), _Ui.Blending_SB, SLOT(setValue(int)));
  connect(_Ui.Blending_SB, SIGNAL(valueChanged(int)), _Ui.Blending_S, SLOT(setValue(int)));

  connect(_Ui.Distance_SB, SIGNAL(valueChanged(int)), this, SLOT(distanceChanged(int)));
  connect(_Ui.Blending_SB, SIGNAL(valueChanged(int)), this, SLOT(blendingChanged(int)));

  connect(_Picture.get(), SIGNAL(pictureChanged()), this, SLOT(pictureChanged()));
}

BandDetectorWidget::~BandDetectorWidget()
{
}

bool BandDetectorWidget::execute()
{
  compute();
  return false;
}

void BandDetectorWidget::distanceChanged(const int inDistance)
{
  _Distance = inDistance;
  compute();
}

void BandDetectorWidget::numberOfMinimumLinesChanged(const int inNumberOfMinimumLines)
{
  _NumOfMinLines = inNumberOfMinimumLines;
  compute();
}

void BandDetectorWidget::blendingChanged(const int inBlending)
{
  _Blending = static_cast<float>(inBlending) / 100.0f;
  compute();
}

void BandDetectorWidget::pictureChanged()
{
  _OriginalImage = _Picture->getMat();
  compute();
}

void BandDetectorWidget::compute()
{
  constexpr float imageDivisor = 1.0f;
  cv::Mat image = _OriginalImage.clone();
  cv::Mat processed;
  cv::resize(
    image,
    processed,
    cv::Size(static_cast<float>(image.cols) / imageDivisor,
      static_cast<float>(image.rows) / imageDivisor));

  if (processed.channels() == 3)
  {
    cv::cvtColor(processed, processed, cv::COLOR_BGR2GRAY);
  }
  cv::normalize(processed, processed, 0, 255, cv::NORM_MINMAX);

  cv::Canny(processed, processed, 60, 150, 3);
  std::vector<cv::Vec2f> lines;
  cv::HoughLines(
    processed,     // image
    lines,         // lines
    10.0,          // rho
    CV_PI / 180.0, // theta
    550,           // threshold
    0,             // srn
    0,             // stn
    0.0,           // min theta
    CV_PI          // max theta
  );


  std::vector<std::pair<cv::Point, cv::Point>> filteredLines;
  for (size_t linesIdx = 0; linesIdx < lines.size(); ++linesIdx)
  {
    float rho = lines[linesIdx][0], theta = lines[linesIdx][1];
    cv::Point pt1, pt2;
    double a = cos(theta), b = sin(theta);
    double x0 = a * rho, y0 = b * rho;
    pt1.x = cvRound(x0 + 1000 * (-b));
    pt1.y = cvRound(y0 + 1000 * (a));
    pt2.x = cvRound(x0 - 1000 * (-b));
    pt2.y = cvRound(y0 - 1000 * (a));

    if (std::abs(pt1.y - pt2.y) > 10) continue;
    pt2.x = image.cols;
    filteredLines.push_back(std::make_pair(pt1, pt2));
  }

  std::sort(filteredLines.begin(), filteredLines.end(),
    [](std::pair<cv::Point, cv::Point> a, std::pair<cv::Point, cv::Point> b)
  {
    return a.first.y > b.first.y;
  });

  cv::Mat lineCanvas = image.clone();
  for (size_t fLinesIdx = 1; fLinesIdx < filteredLines.size(); ++fLinesIdx)
  {
    int dist = std::abs(filteredLines[fLinesIdx - 1].first.y - filteredLines[fLinesIdx].first.y);
    if (std::abs(dist - _Distance) > 10) continue;

    line(lineCanvas, filteredLines[fLinesIdx - 1].first, filteredLines[fLinesIdx - 1].second, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
    line(lineCanvas, filteredLines[fLinesIdx].first, filteredLines[fLinesIdx].second, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
  }

  cv::addWeighted(image, 1.0 - _Blending, lineCanvas, _Blending, 0, image);
  _Picture->setMat(image);
}

}