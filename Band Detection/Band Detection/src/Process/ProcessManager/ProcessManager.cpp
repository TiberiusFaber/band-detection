/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   ProcessManager
 */

#include "ProcessManager.h"

#include "../BandDetector/BandDetectorWidget.h"

ProcessManager::ProcessManager()
{
}


ProcessManager::~ProcessManager()
{
}

void ProcessManager::setup(QTabWidget* inProcessWidgetControll_p, std::shared_ptr<Picture> inPicture)
{
  _ProcessWidgetControll_p = inProcessWidgetControll_p;

  process::BandDetectorWidget* bdw = new process::BandDetectorWidget(inPicture);
  _ProcessWidgetControll_p->addTab(bdw, bdw->getName());
}
