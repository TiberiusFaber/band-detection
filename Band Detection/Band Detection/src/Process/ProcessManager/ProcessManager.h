/*
 * Frontier Microscopy
 * Author:  Tibor Kovacs - tiberiusfaber@gmail.com
 * Date:    2020.06.24
 * Class:   ProcessManager
 * Details: Initialize all the processes and bind them into the tab widget.
 */

#ifndef PROCESS_MANAGER_H
#define PROCESS_MANAGER_H

#include "../ProcessWidget.h"
#include "../../Picture/Picture.h"

#include <QWidget>
#include <QVector>
#include <QTabWidget>

#include <memory>

class ProcessManager
{
public:
  ProcessManager();
  ~ProcessManager();

  void setup(QTabWidget* inProcessWidgetControll_p, std::shared_ptr<Picture> inPicture);

private:
  QTabWidget* _ProcessWidgetControll_p;
};

#endif