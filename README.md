# Band Detector #

Open an image about a phase contrast test slide and detect the horizontal lines of it according to the parameters.

### What is this repository for? ###

This is an interview task to Frontier Microscopy.

### How do I get set up? ###

* Open the Visual Studio 2017
* Setup Qt
* Open the project
* Configure the OpenCV library
* Compile